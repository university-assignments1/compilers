#!/usr/bin/python3
# -*- coding: utf-8 -*- 

import time

'''
Ορισμός αυτομάτου: M = ({k0,k1,k2,k3}, Sourcefile, {$,(,)}, p, k0, $, {k3})

k0: Αρχική κατάσταση.
k1: Κατάσταση αδιέξοδος, αν το αυτόματο βρεθεί σε αυτήν την κατάσταση τότε
	θα παραμείνει μέχρι το τέλος της εισόδου.
k2: Ενδιάμεση κατάσταση από την αρχική και την τελική,
	αν υπάρχουν παρενθέσεις στην στοίβα (δηλαδή εκκρεμούν)
	τότε το αυτόματο επιστρέφει στην κατάσταση k0 αλλιώς
	αλλάζει την κατάσταση σε τελική.
k3: Τελική κατάσταση.

===Κανόνες===
1. p( k0, $, ( ) = ( k0, $( )
2. p( k0, (, ( ) = ( k0, (( )
3. p( k0, (, ) ) = ( k2, e )
4. p( k3, $, ( ) = ( k0, $( )
5. p( k3, $, ) ) = ( k1, $ )
6. p( k2, $, e ) = ( k3, $ )
7. p( k2, (, e ) = ( k0, ( )
'''

file = input("Input File: ")
with open(file,"r") as file:
	input_stream = "".join(file.read().split())


steps = input("Print steps(Y/N); ")
if steps.lower() == "yes" or steps.lower() == "y":
	steps = True
else:
	steps = False


pformat = "{:<15}\t" * 3
print(pformat.format("Stack", "State", "Input symbols"))

stack =['$']
state = "k0"

while True:
	if input_stream == '':
		break

	ch = input_stream[0]
	

	# 1
	if state == "k0" and stack[-1] == '$' and ch == '(':
		stack.append(ch)

	# 2
	elif state == "k0" and stack[-1] == '(' and ch == '(':
		stack.append(ch)

	# 3
	elif state == "k0" and stack[-1] == '(' and ch == ')':
		stack.pop()
		state = "k2"

	# 4
	elif state == "k3" and stack[-1] == '$' and ch == '(':
		stack.append(ch)
		state = "k0"

	# 5
	elif state == "k3" and stack[-1] == '$' and ch == ')':
		state = "k1"
	
	# 6
	if state == "k2" and stack[-1] == '$':
		state = "k3"

	# 7
	elif state == "k2" and stack[-1] == '(':
		state = "k0"
	
	input_stream = input_stream[1:]
	
	if steps:
		print("\033[K", end="\r")
		print(pformat.format(" ".join(list(reversed(stack))[0:10]), state, input_stream[0:50]), flush=True, end="\r")		
		time.sleep(0.2)


print

if state == "k3":
	print("YES")
else:
	print("NO")
