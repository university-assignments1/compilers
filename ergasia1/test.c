#include <stdio.h>

enum {tSize=6};

int paths;
int moves[4][2] = {{-1,0}, {1,0}, {0,1}, {0,-1}};
int rooms[tSize][tSize];
int Paths[tSize];
int NumOfPaths[tSize];
int minDoors = tSize*tSize;
int a=0;

//rooms: (0: den exei mpei, 1: exei mpei)
int Sum() {
	int i,j;
	int s = 0;

	for (i=0;i < tSize;i++) for (j=0;j<tSize;j++) s += rooms[i][j];
	return s;
}
int SumPaths() {
	int i;
	int s=0;
	for (i=0;i<tSize;i++) {
		s+=Paths[i];
	}
	return s;
}
int Min(int atable[]) {
	int min = atable[0];
	int i;
	for (i=1;i<tSize;i++) min = (atable[i] < min)? atable[i]:min;
	return min;
}

//p: position [x,y] (x: grammh, y:sthlh)
void Step(int p[2]) {
	a++;
	int i,m;

	rooms[p[0]][p[1]] = 1;

	if ((p[0] == tSize-1) && (Sum(rooms) == tSize*tSize)) {
		paths += 1;
		if(NumOfPaths[p[1]] == 0) NumOfPaths[p[1]] = 1;
	}
	else {
		int m[2];

		for (i=0;i < 4;i++) {

			int newP[] = {p[0] + moves[i][0], p[1] + moves[i][1]};

			if ((newP[0] >= 0 && newP[0] < tSize) && (newP[1] >=0 && newP[1] < tSize)) {
				if (rooms[newP[0]][newP[1]] == 0) Step(newP);
				else continue;
			}
			else continue;
		}
	}

	rooms[p[0]][p[1]] = 0;
}

int main() {

	int i,j;

	for (i=0;i<tSize;i++) for (j=0;j<tSize;j++) rooms[i][j] = 0;
	for (i=0;i<tSize;i++) NumOfPaths[i] = 0;

	for (i=0;i<tSize;i++) {
		if(!(tSize % 2 == 0)) {
			if((i+1) % 2 == 0) {
				printf("Ta monopatia apo thn %ih eisodo einai: 0\n", i+1);
				continue;
			}
		}
		paths = 0;
		int tmp[2] = {0,i};
		Step(tmp);
		Paths[i] = paths;
		printf("Ta monopatia apo thn %ih eisodo einai: %i\n", i+1, paths);
		printf("%i\n", a);
		a=0;
	}

	printf("Ta sunolika monopatia einai: %i\n", SumPaths());
	printf("Oi elaxistes portes pou xreiazontai einai: %i\n", minDoors + Min(NumOfPaths));
	
	return 0;
}
