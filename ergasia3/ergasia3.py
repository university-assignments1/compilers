#!/usr/bin/python3
# -*- coding: utf-8 -*- 

'''
Γραμματική LL(1)
	S -> [A]
	A -> BE
	B -> x | y | S
	E -> :A | +A | ε
'''

grammar = {
	"<S>": ["[<A>]"],
	"<A>": ["<B><E>"],
	"<B>": ["x", "y", "<S>"],
	"<E>": [":<A>", "+<A>", ""]
}

def FIRST(s):
	if s == "<S>":	return ['[']
	if s == "<A>":	return ['x', 'y', '[']
	if s == "<B>":	return ['x', 'y', '[']
	if s == "<E>":	return [':', '+']


def parser(gen_str):
	global goal_str

	print(gen_str)

	start = gen_str.find('<')

	if start == -1:
		return (goal_str == "")
	else:
		if goal_str == "":
			return False

	rule = gen_str[start:start+3]

	if rule == "<S>" or rule == "<A>":

		if goal_str[0] in FIRST(rule):
			gen_str = gen_str.replace(rule, grammar[rule][0], 1)
		
			if rule == "<S>":	goal_str = goal_str[1:]
		else:
			return False

	elif rule == "<B>" or rule == "<E>":

		if goal_str[0] in FIRST(rule):
			replace_with = grammar[rule][FIRST(rule).index(goal_str[0])]
			gen_str = gen_str.replace(rule, replace_with, 1)
		
			if replace_with != "<S>":	goal_str = goal_str[1:]
		else:
			if rule == "<E>":
				gen_str = gen_str.replace(rule, "", 1)
				goal_str = goal_str[1:]
			else:
				return False

	return parser(gen_str)

goal_str = input("Input string: ")
gen_str = "<S>"

if parser(gen_str):
	print("Parsed")
else:
	print("Not parsed")
