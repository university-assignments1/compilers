#!/usr/bin/python3
# -*- coding: utf-8 -*-

import random as r

# Γραμματική
# <έκφραση> ::= <όρος>|<έκφραση>+<όρος>
# <όρος> ::= <παράγοντας>|<όρος>*<παράγοντας>
# <παράγοντας> ::= a|b|c

grammar = {
	"<έκφραση>": ["<όρος>", "<έκφραση> + <όρος>"],
	"<όρος>": ["<παράγοντας>", "<όρος> * <παράγοντας>"],
	"<παράγοντας>": ["a", "b", "c"]
}

word = "<έκφραση>"

for i in range(20):

	'''
	Βρίσκει την πιο αριστερή αντικατάσταση.
	Αρχικά βρίσκει την τοποθεσία του πιο αριστερού '<'
	και εξάγει από την τοποθεσία αυτήν όλους τους χαρακτήρες μέχρι το '>'.
	Οπότε τελικά θα έχω κάτι τέτοιο '<..>'.
	'''
	p = word.find('<')
	end = word.find('>') + 1
	if p == -1: break
	rp = word[p : end]

	print(word[:p] + "\033[4m" + rp + "\033[0m" + word[end:])

	word = word.replace(rp, grammar[rp][r.randint(0, len(grammar[rp]) - 1)], 1)

if i == 19:

	p = word.find('<')
	while p != -1:
		end = word.find('>') + 1
		rp = word[p : end]

		print(word[:p] + "\033[4m" + word[p:end] + "\033[0m" + word[end:])
		
		word = word.replace(rp, grammar[rp][0], 1)
		
		p = word.find('<')

print(word)