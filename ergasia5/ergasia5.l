%{
	#include <stdio.h>
	#include <locale.h>
	#include <wchar.h>
%}

POINT		Α|Β|Γ|Δ|Ε
TRIANGLE	{POINT}{3}
SQUARE		{POINT}{4}
ANGLE		{POINT}
SHAPE		"τρίγωνο"|"τετράγωνο"|"γωνία"

%%

"Δίνεται "{SHAPE}" "({ANGLE}|{TRIANGLE}|{SQUARE}) {

	char * array[3];
	char * tmp = yytext;
	char * p = strtok(tmp, " ");

	int i = 0;
	while (p != NULL) {
		array[i++] = p;
		p = strtok (NULL, " ");
	}

	setlocale(LC_ALL, "");

	printf("%s : είναι ρήμα\n", array[0]);
	printf("%s : είναι γεωμετρική οντότητα\n", array[1]);

	/*
	 * Μέχρι εδώ είναι το υποερώτημα E.1.
	 * Από εδώ και πέρα γίνεται ο έλεγχος
	 * για το αν οι κορυφές του γεωμετρικού
	 * σχήματος είναι σωστές, δηλαδή διαφορετικοί
	 * χαρακτήρες.
	 *
	 * Χρησιμοποιούμε τον τύπο wide characters (wchar_t)
	 * για να μπορέσουμε να χειριστούμε Ελληνικούς χαρακτήρες.
	 */
	wchar_t points[4];
	mbstowcs(points, array[2], 4);

	int j;
	for (i=0; i < wcslen(points); i++)
	{
		for (j=i+1; j < wcslen(points); j++)
			if (points[i] == points[j]) break;

		if (j < wcslen(points)) break;
	}

	if (i >= wcslen(points))
		printf("%s \t: είναι όνομα γεωμετρικής οντότητας\n", array[2]);
	else
		printf("%s \t: δεν είναι όνομα γεωμετρικής οντότητας\n", array[2]);
}

%%
