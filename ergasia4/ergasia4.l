
CHAR		[a-z]
DIGIT		[0-9]
TYPE		"char"|"boolean"|"integer"|"real"
ID			{CHAR}({DIGIT}?{CHAR}?)*

/* D: Decleration */
D {ID}(","{ID})*":"{TYPE}";"

/* SS: Statements_Section */
SS "var"[\t\n ]*({D}[\t\n ]*)+

%%

{SS} {
	printf("Statements Section\n");
}

%%